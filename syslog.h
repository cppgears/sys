#pragma once
#include <cstdio>
#include <execinfo.h>

namespace snippet {
	namespace sys {
		class syslog {
		public:
			inline static void open(const char* stdout_logfile, const char* stderr_logfile = nullptr) {
				static FILE*	___stdout = nullptr;
				static FILE*	___stderr = nullptr;

				if (stdout_logfile != nullptr && ___stdout == nullptr) {
					___stdout = freopen64(stdout_logfile, "w+t", stdout);
				}

				if (stderr_logfile != nullptr && ___stderr == nullptr) {
					___stderr = freopen64(stderr_logfile, "w+t", stderr);
				}
			}
			inline static void backtrace(const char* space, FILE* file = stderr) {
				void *buffer[100];
				char **strings;
				int nptrs = ::backtrace(buffer, 100);
				fprintf(file, "[ %s ] backtrace() returned %d addresses\n", space, nptrs);
				strings = ::backtrace_symbols(buffer, nptrs);
				if (strings == nullptr) { fprintf(file, "[ %s ] backtrace_symbols() no backtrace symbols\n", space); }
				else {
					for (int j = 0; j < nptrs; j++)	fprintf(file, "%s\n", strings[j]);
					free(strings);
				}
			}
		};
	}
}